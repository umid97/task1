$(function(){
    $('#delItems').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var check = $('.items');
        var array = [];
        var s = 0;
        for(var i = 0; i < check.length; i++){
            if(check[i].checked){
                array.push(check[i].value);
                s++;
            }
        }
        if(s === 0)
        {
            alert('Malumotlarni oqib bolmadi!');
            return;
        }
        $.ajax({
            url: 'http://localhost/task1/admin/users/del',
            type: 'GET',
            data: {array: array, id: id},
        });

        $.ajax({
            url: 'http://localhost/task1/admin/users/all',
            type: 'GET',
            data: {id: id},
            success: function(r){
                if(!r)
                    return false;
                $('.table-striped tbody').html(r);
            }
        });
    });

    var old_items = [];
    var old_users_id = [];

    // update
    $('#updateItems').click(function(res){
        res.preventDefault();
        var id = $(this).data('id');
        var check = $('.items-update');
        var array = [];
        var s = 0;
        for(var i = 0; i < check.length; i++){
            if(check[i].checked){
                array.push(check[i].value);
                s++;
            }
        }
        if(s === 0)
        {
            alert('Malumotlarni oqib bolmadi!');
            return;
        }
        $.ajax({
            data: {array: array, id: id},
            url: 'http://localhost/task1/admin/users/update-all',
            type: 'GET',
            success: function(r){
                if(!r)
                    return false;
                $('.modal-body .container').html(r);
                var old_item = $('.item_name');
                var old_user_id = $('.user_id');
                old_item.each((index, items) => {
                   old_items.push(items.value);
                });
                old_user_id.each((index, items) => {
                    old_users_id.push(items.value);
                });
            }
        });

        $('#my-modal').modal('show').find('#modal-content').load();
    });

    $('#my-modal').on('click', '#myButton', function(r){
        r.preventDefault();
        var itemName = $('select.item_name');
        var userId = $('select.user_id');
        var id = $('#selection').data('id');
        var item = [];
        var user = [];
        itemName.each((index, data) => {
            item.push(data.value);
        });

        userId.each((index, data) => {
            user.push(data.value);
        });

        $.ajax({
           data: {items: item, users: user, id: id, old_items: old_items, old_users: old_users_id},
           url: 'http://localhost/task1/admin/users/update-data',
           type: 'GET',
           success: function(d){
               if(!d)
                   return false;
               console.log(d);
           }
        });
    });
});