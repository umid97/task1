<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%contragent}}`.
 */
class m200603_113952_create_contragent_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%contragent}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'address' => $this->char(50),
            'phone' => $this->char(20),
            'directory' => $this->char(50),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%contragent}}');
    }
}
