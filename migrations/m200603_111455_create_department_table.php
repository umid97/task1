<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%department}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%department_type}}`
 */
class m200603_111455_create_department_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%department}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'type_id' => $this->integer(),
            'parent_id' => $this->integer(),
            'address' => $this->string(100),
            'international_currency' => $this->float(),
            'default_currency' => $this->float(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `type_id`
        $this->createIndex(
            '{{%idx-department-type_id}}',
            '{{%department}}',
            'type_id'
        );

        // add foreign key for table `{{%department_type}}`
        $this->addForeignKey(
            '{{%fk-department-type_id}}',
            '{{%department}}',
            'type_id',
            '{{%department_type}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%department_type}}`
        $this->dropForeignKey(
            '{{%fk-department-type_id}}',
            '{{%department}}'
        );

        // drops index for column `type_id`
        $this->dropIndex(
            '{{%idx-department-type_id}}',
            '{{%department}}'
        );

        $this->dropTable('{{%department}}');
    }
}
