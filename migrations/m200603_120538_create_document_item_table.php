<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%document_item}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%item}}`
 * - `{{%document}}`
 */
class m200603_120538_create_document_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%document_item}}', [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer(),
            'quantity' => $this->smallInteger(),
            'income_price' => $this->float(),
            'wh_price' => $this->float(),
            'selling_price' => $this->float(),
            'currency' => $this->float(),
            'lot' => $this->char(100),
            'document_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `item_id`
        $this->createIndex(
            '{{%idx-document_item-item_id}}',
            '{{%document_item}}',
            'item_id'
        );

        // add foreign key for table `{{%item}}`
        $this->addForeignKey(
            '{{%fk-document_item-item_id}}',
            '{{%document_item}}',
            'item_id',
            '{{%item}}',
            'id',
            'CASCADE'
        );

        // creates index for column `document_id`
        $this->createIndex(
            '{{%idx-document_item-document_id}}',
            '{{%document_item}}',
            'document_id'
        );

        // add foreign key for table `{{%document}}`
        $this->addForeignKey(
            '{{%fk-document_item-document_id}}',
            '{{%document_item}}',
            'document_id',
            '{{%document}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%item}}`
        $this->dropForeignKey(
            '{{%fk-document_item-item_id}}',
            '{{%document_item}}'
        );

        // drops index for column `item_id`
        $this->dropIndex(
            '{{%idx-document_item-item_id}}',
            '{{%document_item}}'
        );

        // drops foreign key for table `{{%document}}`
        $this->dropForeignKey(
            '{{%fk-document_item-document_id}}',
            '{{%document_item}}'
        );

        // drops index for column `document_id`
        $this->dropIndex(
            '{{%idx-document_item-document_id}}',
            '{{%document_item}}'
        );

        $this->dropTable('{{%document_item}}');
    }
}
