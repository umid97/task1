<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%currency}}`.
 */
class m200605_140401_add_updated_by_column_to_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%currency}}', 'updated_by', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%currency}}', 'updated_by');
    }
}
