<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%document}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%contragent}}`
 * - `{{%document_type}}`
 */
class m200603_114022_create_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%document}}', [
            'id' => $this->primaryKey(),
            'doc_number' => $this->integer(),
            'reg_date' => $this->datetime(),
            'from_department' => $this->char(100),
            'to_department' => $this->char(100),
            'contragent_id' => $this->integer(),
            'doc_type_id' => $this->integer(),
            'status' => $this->smallInteger(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `contragent_id`
        $this->createIndex(
            '{{%idx-document-contragent_id}}',
            '{{%document}}',
            'contragent_id'
        );

        // add foreign key for table `{{%contragent}}`
        $this->addForeignKey(
            '{{%fk-document-contragent_id}}',
            '{{%document}}',
            'contragent_id',
            '{{%contragent}}',
            'id',
            'CASCADE'
        );

        // creates index for column `doc_type_id`
        $this->createIndex(
            '{{%idx-document-doc_type_id}}',
            '{{%document}}',
            'doc_type_id'
        );

        // add foreign key for table `{{%document_type}}`
        $this->addForeignKey(
            '{{%fk-document-doc_type_id}}',
            '{{%document}}',
            'doc_type_id',
            '{{%document_type}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%contragent}}`
        $this->dropForeignKey(
            '{{%fk-document-contragent_id}}',
            '{{%document}}'
        );

        // drops index for column `contragent_id`
        $this->dropIndex(
            '{{%idx-document-contragent_id}}',
            '{{%document}}'
        );

        // drops foreign key for table `{{%document_type}}`
        $this->dropForeignKey(
            '{{%fk-document-doc_type_id}}',
            '{{%document}}'
        );

        // drops index for column `doc_type_id`
        $this->dropIndex(
            '{{%idx-document-doc_type_id}}',
            '{{%document}}'
        );

        $this->dropTable('{{%document}}');
    }
}
