<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%item_category}}`.
 */
class m200603_111557_create_item_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%item_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'parent_id' => $this->integer()->defaultValue(NULL),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%item_category}}');
    }
}
