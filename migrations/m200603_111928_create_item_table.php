<?php

use yii\db\Migration;

class m200603_111928_create_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%item}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'article' => $this->char(50),
            'category_id' => $this->integer(),
            'wieght' => $this->float(),
            'unit_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `category_id`
        $this->createIndex(
            '{{%idx-item-category_id}}',
            '{{%item}}',
            'category_id'
        );

        // add foreign key for table `{{%item_category}}`
        $this->addForeignKey(
            '{{%fk-item-category_id}}',
            '{{%item}}',
            'category_id',
            '{{%item_category}}',
            'id',
            'CASCADE'
        );

        // creates index for column `unit_id`
        $this->createIndex(
            '{{%idx-item-unit_id}}',
            '{{%item}}',
            'unit_id'
        );

        // add foreign key for table `{{%unit}}`
        $this->addForeignKey(
            '{{%fk-item-unit_id}}',
            '{{%item}}',
            'unit_id',
            '{{%unit}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%item_category}}`
        $this->dropForeignKey(
            '{{%fk-item-category_id}}',
            '{{%item}}'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            '{{%idx-item-category_id}}',
            '{{%item}}'
        );

        // drops foreign key for table `{{%unit}}`
        $this->dropForeignKey(
            '{{%fk-item-unit_id}}',
            '{{%item}}'
        );

        // drops index for column `unit_id`
        $this->dropIndex(
            '{{%idx-item-unit_id}}',
            '{{%item}}'
        );

        $this->dropTable('{{%item}}');
    }
}
