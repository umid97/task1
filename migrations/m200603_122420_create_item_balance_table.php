<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%item_balance}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%item}}`
 * - `{{%document_item}}`
 * - `{{%document}}`
 * - `{{%department}}`
 */
class m200603_122420_create_item_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%item_balance}}', [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer(),
            'lot' => $this->char(50),
            'quantity' => $this->decimal(20,3),
            'inventory' => $this->decimal(20,3),
            'document_item_id' => $this->integer(),
            'document_id' => $this->integer(),
            'department_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `item_id`
        $this->createIndex(
            '{{%idx-item_balance-item_id}}',
            '{{%item_balance}}',
            'item_id'
        );

        // add foreign key for table `{{%item}}`
        $this->addForeignKey(
            '{{%fk-item_balance-item_id}}',
            '{{%item_balance}}',
            'item_id',
            '{{%item}}',
            'id',
            'CASCADE'
        );

        // creates index for column `document_item_id`
        $this->createIndex(
            '{{%idx-item_balance-document_item_id}}',
            '{{%item_balance}}',
            'document_item_id'
        );

        // add foreign key for table `{{%document_item}}`
        $this->addForeignKey(
            '{{%fk-item_balance-document_item_id}}',
            '{{%item_balance}}',
            'document_item_id',
            '{{%document_item}}',
            'id',
            'CASCADE'
        );

        // creates index for column `document_id`
        $this->createIndex(
            '{{%idx-item_balance-document_id}}',
            '{{%item_balance}}',
            'document_id'
        );

        // add foreign key for table `{{%document}}`
        $this->addForeignKey(
            '{{%fk-item_balance-document_id}}',
            '{{%item_balance}}',
            'document_id',
            '{{%document}}',
            'id',
            'CASCADE'
        );

        // creates index for column `department_id`
        $this->createIndex(
            '{{%idx-item_balance-department_id}}',
            '{{%item_balance}}',
            'department_id'
        );

        // add foreign key for table `{{%department}}`
        $this->addForeignKey(
            '{{%fk-item_balance-department_id}}',
            '{{%item_balance}}',
            'department_id',
            '{{%department}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%item}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-item_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `item_id`
        $this->dropIndex(
            '{{%idx-item_balance-item_id}}',
            '{{%item_balance}}'
        );

        // drops foreign key for table `{{%document_item}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-document_item_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `document_item_id`
        $this->dropIndex(
            '{{%idx-item_balance-document_item_id}}',
            '{{%item_balance}}'
        );

        // drops foreign key for table `{{%document}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-document_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `document_id`
        $this->dropIndex(
            '{{%idx-item_balance-document_id}}',
            '{{%item_balance}}'
        );

        // drops foreign key for table `{{%department}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-department_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `department_id`
        $this->dropIndex(
            '{{%idx-item_balance-department_id}}',
            '{{%item_balance}}'
        );

        $this->dropTable('{{%item_balance}}');
    }
}
