<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%department_type}}`.
 */
class m200603_111420_create_department_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%department_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%department_type}}');
    }
}
