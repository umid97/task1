<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>
    <?= TabularInput::widget([
        'models' => $models,
        'attributeOptions' => [
            'enableAjaxValidation'      => true,
            'enableClientValidation'    => false,
            'validateOnChange'          => false,
            'validateOnSubmit'          => true,
            'validateOnBlur'            => false,
        ],
        'columns' => [
            [
                'name'  => 'title',
                'title' => 'Title',
                'type'  => \unclead\multipleinput\MultipleInputColumn::TYPE_TEXT_INPUT,
            ],
            [
                'name'  => 'description',
                'title' => 'Description',
            ],
            [
                'name'  => 'file',
                'title' => 'File',
                'type'  => \vova07\fileapi\Widget::className(),
                'options' => [
                    'settings' => [
                        'url' => ['site/fileapi-upload']
                    ]
                ],
                'columnOptions' => [
                    'style' => 'width: 250px;',
                    'class' => 'custom-css-class'
                ]
            ],
            [
                'name'  => 'date',
                'type'  => \kartik\date\DatePicker::className(),
                'title' => 'Day',
                'options' => [
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy',
                        'todayHighlight' => true
                    ]
                ],
                'headerOptions' => [
                    'style' => 'width: 250px;',
                    'class' => 'day-css-class'
                ]
            ],
        ],
    ]) ?>

</div>
