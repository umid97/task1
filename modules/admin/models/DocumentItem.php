<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "{{%document_item}}".
 *
 * @property int $id
 * @property int|null $item_id
 * @property int|null $quantity
 * @property float|null $income_price
 * @property float|null $wh_price
 * @property float|null $selling_price
 * @property float|null $currency
 * @property string|null $lot
 * @property int|null $document_id
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property Document $document
 * @property Item $item
 * @property ItemBalance[] $itemBalances
 */
class DocumentItem extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%document_item}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'quantity', 'document_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['income_price', 'wh_price', 'selling_price', 'currency'], 'number'],
            [['lot'], 'string', 'max' => 100],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['document_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'quantity' => 'Quantity',
            'income_price' => 'Income Price',
            'wh_price' => 'Wh Price',
            'selling_price' => 'Selling Price',
            'currency' => 'Currency',
            'lot' => 'Lot',
            'document_id' => 'Document ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemBalances()
    {
        return $this->hasMany(ItemBalance::className(), ['document_item_id' => 'id']);
    }
}
