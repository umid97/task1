<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "{{%item_balance}}".
 *
 * @property int $id
 * @property int|null $item_id
 * @property string|null $lot
 * @property int|null $quantity
 * @property float|null $inventory
 * @property int|null $document_item_id
 * @property int|null $document_id
 * @property int|null $department_id
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property Department $department
 * @property Document $document
 * @property DocumentItem $documentItem
 * @property Item $item
 */
class ItemBalance extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%item_balance}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'quantity', 'document_item_id', 'document_id', 'department_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['inventory'], 'number'],
            [['lot'], 'string', 'max' => 50],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['document_id' => 'id']],
            [['document_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentItem::className(), 'targetAttribute' => ['document_item_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'lot' => 'Lot',
            'quantity' => 'Quantity',
            'inventory' => 'Inventory',
            'document_item_id' => 'Document Item ID',
            'document_id' => 'Document ID',
            'department_id' => 'Department ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentItem()
    {
        return $this->hasOne(DocumentItem::className(), ['id' => 'document_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }
}
