<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Document;

/**
 * DocumentSearch represents the model behind the search form of `app\modules\admin\models\Document`.
 */
class DocumentSearch extends Document
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'doc_number', 'contragent_id', 'doc_type_id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['reg_date', 'from_department', 'to_department'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Document::find();
        $query->leftJoin('document_type','document_type.id = document.doc_type_id');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

//        $query->joinWith('departs');
//        $query->joinWith('depart');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

//        $query->joinWith('departs');
        $token = self::DOC_TYPE_KIRIM;
        if(!empty($params['token'])){
            $token = $params['token'];
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'doc_number' => $this->doc_number,
            'reg_date' => $this->reg_date,
            'document_type.token' => $token,
            'contragent_id' => $this->contragent_id,
            'doc_type_id' => $this->doc_type_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'from_department', $this->from_department])
            ->andFilterWhere(['like', 'to_department', $this->to_department]);

        return $dataProvider;
    }
}
