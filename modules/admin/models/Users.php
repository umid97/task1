<?php

namespace app\modules\admin\models;

use app\models\User;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use Yii;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;
use yii\db\Expression;
use app\components\behaviors\CutomBehaviors;
use yii\helpers\VarDumper;
use yii\web\IdentityInterface;

class Users extends BaseModel implements IdentityInterface
{
    public $item_name;

    public static function tableName()
    {
        return '{{%users}}';
    }

    public function rules()
    {
        return [
            [['name', 'surname', 'username', 'password'], 'required'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 70],
            [['surname'], 'string', 'max' => 60],
            [['username', 'password'], 'string', 'max' => 100],
            [['username'], 'unique'],
            [['item_name'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'username' => 'Username',
            'password' => 'Password',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
    }

    public static function findByUsername($username)
    {
        return self::findOne([
            'username' => $username
        ]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
//        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
//        return $this->authKey === $authKey;
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function getUserDepartments()
    {
        return $this->hasMany(UserDepartment::className(), ['user_id' => 'id']);
    }

    public function getShow($d){
        $i = 0;
        $str = '';
        foreach ($d as $r){
            $str .= "<tr data-key='".$i."'><td>".$r['item_name']."</td><td><input type='checkbox' value=".$r['item_name']." class='items'></td><td><input type='checkbox' value=".$r['item_name']." class='items-update'></td></tr>";
            $i++;
        }
        return $str;
    }

}
