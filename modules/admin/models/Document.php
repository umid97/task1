<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%document}}".
 *
 * @property int $id
 * @property int|null $doc_number
 * @property string|null $reg_date
 * @property string|null $from_department
 * @property string|null $to_department
 * @property int|null $contragent_id
 * @property int|null $doc_type_id
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property Contragent $contragent
 * @property DocumentType $docType
 * @property DocumentItem[] $documentItems
 * @property ItemBalance[] $itemBalances
 */
class Document extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%document}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doc_number', 'contragent_id', 'doc_type_id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['reg_date'], 'safe'],
            [['from_department', 'to_department'], 'string', 'max' => 100],
            [['contragent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contragent::className(), 'targetAttribute' => ['contragent_id' => 'id']],
            [['doc_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentType::className(), 'targetAttribute' => ['doc_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'doc_number' => 'Doc Number',
            'reg_date' => 'Reg Date',
            'from_department' => 'From Department',
            'to_department' => 'To Department',
            'contragent_id' => 'Contragent ID',
            'doc_type_id' => 'Doc Type ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContragent()
    {
        return $this->hasOne(Contragent::className(), ['id' => 'contragent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocType()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'doc_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentItems()
    {
        return $this->hasMany(DocumentItem::className(), ['document_id' => 'id']);
    }

    public function getDeparts(){
        return $this->hasOne(Department::className(), ['id' => 'from_department']);
    }

    public function getDepart(){
        return $this->hasOne(Department::className(), ['id' => 'to_department']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemBalances()
    {
        return $this->hasMany(ItemBalance::className(), ['document_id' => 'id']);
    }

    public function getDepartment($id = null)
    {
        if(!empty($id)){
            $department = Department::find()->asArray()->all();
            return ArrayHelper::map($department, 'id', 'name');
        }
        $department = Department::find()->where(['id' => $id])->asArray()->one();
        return $department['name'];
    }

    public function getContragents($id = null)
    {
        if(!empty($id)){
            $department = Contragent::find()->asArray()->all();
            return ArrayHelper::map($department, 'id', 'name');
        }
        $department = Contragent::find()->where(['id' => $id])->asArray()->one();
        return $department['name'];
    }

    // one data
    public function getOnedata($id){
        $row = self::findOne($id);
        return $row;
    }

}
