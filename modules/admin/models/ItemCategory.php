<?php

namespace app\modules\admin\models;

use Yii;

class ItemCategory extends BaseModel
{
    public static function tableName()
    {
        return '{{%item_category}}';
    }

    public function rules()
    {
        return [
            [['parent_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => ItemCategory::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parent_id' => 'Parent ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function getItems()
    {
        return $this->hasMany(Item::className(), ['category_id' => 'id']);
    }

    public function getParent()
    {
        return $this->hasOne(ItemCategory::className(), ['id' => 'parent_id']);
    }

    public function getItemCategories()
    {
        return $this->hasMany(ItemCategory::className(), ['parent_id' => 'id']);
    }
}
