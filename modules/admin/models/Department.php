<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "{{%department}}".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $type_id
 * @property int|null $parent_id
 * @property string|null $address
 * @property int|null $international_currency
 * @property int|null $default_currency
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property Department $id0
 * @property Department $department
 * @property Currency $internationalCurrency
 * @property Currency $defaultCurrency
 * @property DepartmentType $type
 * @property ItemBalance[] $itemBalances
 * @property UserDepartment[] $userDepartments
 */
class Department extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%department}}';
    }

    public function rules()
    {
        return [
            [['type_id', 'parent_id', 'international_currency', 'default_currency', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['address'], 'string', 'max' => 100],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['id' => 'parent_id']],
            [['international_currency'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['international_currency' => 'id']],
            [['default_currency'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['default_currency' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepartmentType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type_id' => 'Type ID',
            'parent_id' => 'Parent ID',
            'address' => 'Address',
            'international_currency' => 'International Currency',
            'default_currency' => 'Default Currency',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Department::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInternationalCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'international_currency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'default_currency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(DepartmentType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemBalances()
    {
        return $this->hasMany(ItemBalance::className(), ['department_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDepartments()
    {
        return $this->hasMany(UserDepartment::className(), ['department_id' => 'id']);
    }
}
