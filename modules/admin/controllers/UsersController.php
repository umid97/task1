<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\AuthAssignment;
use app\modules\admin\models\AuthItem;
use Yii;
use app\modules\admin\models\Users;
use app\modules\admin\models\UsersSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Application;

class UsersController extends BaseController
{
    public function behaviors()
    {
        parent::behaviors();
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['Administrator'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionOne(){
        $id = Yii::$app->request->get('id');

        return $this->render('one', compact('id'));
    }

    public function actionCreate()
    {
        $model = new Users();
        $assig = AuthItem::find()
            ->asArray()
            ->all();

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->session->set('username', $model->username);
            $model->setPassword($model->password);
            $model->save();

            return $this->redirect(['users/add-assignment']);
        }

        return $this->render('create', [
            'model' => $model,
            'assig' => $assig,
        ]);
    }

    public function actionDel(){
        $model = new Users();
        $id = Yii::$app->request->get('id');
        $array = Yii::$app->request->get('array');
        if(Yii::$app->request->isAjax)
            $this->layout = false;
        $sql = "DELETE FROM auth_assignment WHERE user_id = $id AND ";
        foreach ($array as $r){
            $sql .= " item_name = '{$r}' OR ";
        }
        $sql = rtrim($sql, 'OR ');
        $res = Yii::$app->db->createCommand($sql);
        $res->queryAll();
        $data = Yii::$app->db->createCommand("SELECT * FROM auth_assignment WHERE user_id = $id");
        $r = $data->queryAll();
        return $this->render('del', compact('r'));
    }

    public function actionAll(){
        $model = new Users();
        $id = Yii::$app->request->get('id');
        $row = AuthAssignment::find()
            ->asArray()
            ->where(['user_id' => $id])
            ->all();
        if(empty($row)) {
            $row = '<tr></tr>';
            return $row;
        }
        return $model->getShow($row);
    }

    public function actionUpdateAll(){
        $id = Yii::$app->request->get('id');
        $array = Yii::$app->request->get('array');
        $sql = "SELECT * FROM auth_assignment WHERE user_id = $id AND ";
        foreach($array as $r){
            $sql .= " item_name = '{$r}' OR ";
        }
        if(Yii::$app->request->isAjax)
        {
            $this->layout = false;
        }
        $item = AuthItem::find()->asArray()->all();
        $user = Users::find()->asArray()->all();
        $sql = rtrim($sql, 'OR ');
        $result = Yii::$app->db->createCommand($sql);
        $res = $result->queryAll();
        return $this->render('update_all', compact('res', 'item', 'user'));
    }

    public function actionUpdateData(){
        $id = Yii::$app->request->get('id');
        $items = Yii::$app->request->get('items');
        $users = Yii::$app->request->get('users');
        $old_users = Yii::$app->request->get('old_users');
        $old_items = Yii::$app->request->get('old_items');
        debug($old_users);
        debug($old_items);
        $i = 0;
        $sql = "SELECT * FROM auth_assignment WHERE user_id = $id AND ";
        foreach($old_users as $r){
            $sql .= " user_id = $r AND item_name = '{$old_items[$i]}' AND ";
            $i++;
        }
        $sql = rtrim($sql, ' AND ');
        $result = Yii::$app->db->createCommand($sql);
        $res = $result->queryAll();
        $sum = 0;
        $query = "INSERT INTO auth_assignment (item_name, user_id) VALUES (";
        foreach($res as $r){
            $query .= "'".addslashes($items[$sum])."', '".addslashes($users[$sum])."'), (";
            $sum++;
        }
        $query = rtrim($query, ', (');
        $res = Yii::$app->db->createCommand($query);
        $all = $res->queryAll();
        return true;
    }

    public function actionAddAssignment(){
        $model = new AuthAssignment();
        $item = AuthItem::find()
            ->asArray()
            ->all();
        $users = Users::find()
            ->asArray()
            ->where(['username' => Yii::$app->session->get('username')])
            ->all();

        if($model->load(Yii::$app->request->post()) && $model->save()){
            Yii::$app->session->setFlash('success', 'Foydalanuvchiga huquq berildi!');
            return $this->redirect(['auth-assignment/index']);
        }

        return $this->render('assig', compact('model', 'users', 'item'));
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
