<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Contragent;
use app\modules\admin\models\Department;
use app\modules\admin\models\DocumentItem;
use app\modules\admin\models\DocumentType;
use app\modules\admin\models\ItemBalance;
use Yii;
use app\modules\admin\models\Document;
use app\modules\admin\models\DocumentSearch;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DocumentController implements the CRUD actions for Document model.
 */
class DocumentController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        $token = Yii::$app->request->get('token');
        $this->token = Yii::$app->request->get('token', $token);
        return parent::actions();
    }

    /**
     * Lists all Document models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DocumentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Document model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $data = DocumentItem::find()
            ->asArray()
            ->where(['document_id' => $id])
            ->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'data' => $data,
        ]);
    }

    /**
     * Creates a new Document model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Document();
        $models = [new DocumentItem()];
        $contragent = Contragent::find()->asArray()->all();
        $document_type = DocumentType::find()->asArray()->all();
        $data = Yii::$app->request->post();
        if(Yii::$app->request->isPost){
            try{
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                $model->status = $model::STATUS_ACTIVE;
                $docType = DocumentType::find()->where(['token' => $this->token])->asArray()->one();
                $docTypeId = 1;
                if(!empty($docType)){
                    $docTypeId = $docType['id'];
                }
                $data['Document']['doc_type_id'] = $docTypeId;
                 if($model->load($data) && $model->save()){

                     $saved = true;
                     $documentId = $model->id;
                     if(!empty($data['DocumentItem'])){
                         foreach ($data['DocumentItem'] as $item){
                             $modelDocItem = new DocumentItem();
                             $modelDocItem->setAttributes([
                                'item_id' => $item['item_id'],
                                'quantity' => $item['quantity'],
                                'document_id' => $documentId,
                                'income_price' => $item['income_price'],
                                'wh_price' => $item['wh_price'],
                                'selling_price' => $item['selling_price'],
                                'currency' => $item['currency'],
                                'lot' => $item['lot'],
                             ]);
                             if($modelDocItem->save()){
                                 $saved = true;
                             }
                             else{
                                 $saved = false;
                                 break;
                             }
                         }
                     }

                     if($saved){
                         $transaction->commit();
                         Yii::$app->session->setFlash('saccess', 'Saved successfully....');
                         return $this->redirect(['view', 'id' => $model->id]);
                     }
                     else{
                        $transaction->rollBack();
                     }
                 }
            }catch(Exception $e){
                Yii::info('Error Data'.$e->getMessage(), 'save');
            }
        }

        return $this->render('create', [
            'model' => $model,
            'document_type' => $document_type,
            'models' => $models,

        ]);
    }

    public function actionFinish()
    {
        $token = Yii::$app->request->get('token');
        $id = Yii::$app->request->get('id');
        if(empty($id))
            throw new ForbiddenHttpException('Parametr mavjud emas!');
        $model = $this->findModel($id);
        $model->status = $model::STATUS_ENDACTIVE;
        if($token == 'KIRIM'){
            $to_department = $model->to_department;
        }
        elseif($token == 'CHIQIM'){
            $to_department = $model->from_department;
        }
        elseif($token == 'KOCHIRISH'){
            $to_department = $model->from_department;
        }

        if(!empty($model->documentItems)){
            $models = $model->documentItems;
        }
        else{
            $models = [new DocumentItem()];
        }
        $saved = false;
        $error = [];
        $success = 'SuccessFully Add';
        if(!empty($models)){
            $transaction = Yii::$app->db->beginTransaction();
            try{
                foreach ($models as $r){
                    $itemBalance = new ItemBalance();
                    $result = ItemBalance::find()
                        ->asArray()
                        ->where(['item_id' => $r['item_id']])
                        ->andWhere(['lot' => $r['lot']])
                        ->andWhere(['department_id' => $to_department])
                        ->orderBy(['id' => SORT_DESC])
                        ->one();
                    // Agar malumotlar bolsa unda if ishledi va kirim bolsa qoshadi chiqim bolsa ayirishni yozish kerek kochirish bolsa ayirish agar maxsulot bolmasa unda xatolik bersin
                    if(!empty($result)){
                        if($token === 'CHIQIM'){
                            if($result['inventory'] < $r['quantity']){
                                $error = array_push($error, $result['item_id']);
                                $itemBalance = null;
                            }
                            else{
                                $itemBalance->setAttributes([
                                    'item_id' => $result['item_id'],
                                    'lot' => $result['lot'],
                                    'quantity' => ($r['quantity']*(-1)),
                                    'inventory' => $result['inventory'] - $r['quantity'],
                                    'document_item_id' => $r['id'],
                                    'document_id' => $r['document_id'],
                                    'department_id' => $result['department_id'],
                                ]);
                            }
                        }
                        elseif($token === 'KIRIM'){
                                $itemBalance->setAttributes([
                                    'item_id' => $result['item_id'],
                                    'lot' => $result['lot'],
                                    'quantity' => $r['quantity'],
                                    'inventory' => $result['inventory'] + $r['quantity'],
                                    'document_item_id' => $r['id'],
                                    'document_id' => $r['document_id'],
                                    'department_id' => $result['department_id'],
                                ]);
                        }
                        elseif ($token === 'KOCHIRISH'){
                            if($result['inventory'] < $r['quantity']){
                                $error = array_push($error, $result['item_id']);
                                $itemBalance = null;
                            }
                            else{
                                $itemBalance->setAttributes([
                                    'item_id' => $result['item_id'],
                                    'lot' => $result['lot'],
                                    'quantity' => ($r['quantity']*(-1)),
                                    'inventory' => $result['inventory'] - $r['quantity'],
                                    'document_item_id' => $r['id'],
                                    'document_id' => $r['document_id'],
                                    'department_id' => $result['department_id'],
                                ]);
                            }
                        }
                    }
                    // agar maxsulot bolmasa unda xatolik bersin else ishledi
                    else{
                        if($token == 'KIRIM'){
                            $itemBalance->setAttributes([
                                'item_id' => $r['item_id'],
                                'lot' => $r['lot'],
                                'quantity' => $r['quantity'],
                                'inventory' => $r['quantity'],
                                'document_item_id' => $r['id'],
                                'document_id' => $r['document_id'],
                                'department_id' => $to_department,
                            ]);
                        }
                        elseif($token == 'CHIQIM'){
                            Yii::$app->session->setFlash('success', 'Kechirasiz bizda hozircha bunday maxsulotimiz mavjud emas!!!');
                            $saved = false;
                            break;
                        }
                        elseif ($token === 'KOCHIRISH'){
                            Yii::$app->session->setFlash('success', 'Kechirasiz bizda hozircha bunday maxsulotimiz mavjud emas!!!');
                            $saved = false;
                            break;
                        }
                    }

                    if($itemBalance === null){
                        Yii::$app->session->setFlash('success', 'Maxsulot miqdori kop! Skladda maxsulot miqdori kam...');
                        $saved = false;
                    }
                    else{
                        if($itemBalance->save()){
                            $saved = true;
                        }
                        else{
                            $saved = false;
                            break;
                        }
                    }
                }
                if($saved && $model->save()){
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', $success);
                    return $this->redirect(['view', 'id' => $model->id]);
                }
                else{
                    $transaction->rollBack();
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            catch(Exception $e){
                Yii::info('Error Message '.$e->getMessage());
            }
        }
        else{
            Yii::$app->session->setFlash('error', 'Qiymat kelmadi!...');
            return $this->redirect(['view', 'id' => $model->id]);
        }
//        $model = new Document();
//        $data = $model->getOnedata($id);
//        $data->status = 3;
//        debug($data);
//        exit();
//        if($data->save()){
//            return $this->redirect(['document/index']);
//        }
//        else{
//            return $this->redirect(Yii::$app->request->referrer);
//        }
    }

    /**
     * Updates an existing Document model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->status == 3){
            throw new ForbiddenHttpException('Sizga bu sahifaga kirishga ruxsat etilmadi!');
        }
        if(!empty($model->documentItems))
            $models = $model->documentItems;
        else
            $models = [new DocumentItem()];

        $data = Yii::$app->request->post();
        if(Yii::$app->request->isPost){
            try{
                $saved = false;
                $transaction = Yii::$app->db->beginTransaction();
                if($model->load($data) && $model->save()){
                    $documentId = $model->id;
                    $saved = true;
                    if(!empty($data['DocumentItem'])){

                        DocumentItem::deleteAll(['document_id' => $id]);
                        foreach ($data['DocumentItem'] as $item){
                            $docItem = new DocumentItem();
                            $docItem->setAttributes([
                                'item_id' => $item['item_id'],
                                'quantity' => $item['quantity'],
                                'document_id' => $documentId,
                                'income_price' => $item['income_price'],
                                'wh_price' => $item['wh_price'],
                                'selling_price' => $item['selling_price'],
                                'currency' => $item['currency'],
                                'lot' => $item['lot'],
                            ]);
                            if($docItem->save()){
                                $saved = true;
                            }else{
                                $saved = false;
                                break;
                            }
                        }
                    }
                    if($saved){
                        $transaction->commit();
                        Yii::$app->session->set('success', 'Successfull Update');
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                    else{
                        $transaction->rollBack();
                    }
                }
            }catch(Exception $e){
                 Yii::info('Error Message '.$e->getMessage());
            }
        }


        $contragent = Contragent::find()
            ->asArray()
            ->all();
        $document_type = DocumentType::find()
            ->asArray()
            ->all();

        return $this->render('update', compact('models', 'model', 'contragent', 'document_type'));

//-------------------------------------------
//        $documentItem = DocumentItem::find()
//            ->asArray()
//            ->where(['document_id' => $id])
//            ->all();
//
//        $models = [new DocumentItem()];
//        $contragent = Contragent::find()->asArray()->all();
//        $document_type = DocumentType::find()->asArray()->all();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        }
//
//        return $this->render('update', [
//            'model' => $model,
//            'contragent' => $contragent,
//            'document_type' => $document_type,
//            'models' => $models,
//            'documentItem' => $documentItem
//        ]);
    }

    /**
     * Deletes an existing Document model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->status == 3){
            throw new ForbiddenHttpException('Sizga bu sahifaga kirishga ruxsat etilmadi!');
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Document model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Document the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Document::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
