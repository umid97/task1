<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Currency;
use app\modules\admin\models\DepartmentType;
use Yii;
use app\modules\admin\models\Department;
use app\modules\admin\models\DepartmentSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class DepartmentController extends BaseController
{

    public function actionIndex()
    {
        $searchModel = new DepartmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($id)
    {
        if(!Yii::$app->user->can('department/view')){
            throw new ForbiddenHttpException('Sizga ruxsat berilmagan!');
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        if(!Yii::$app->user->can('department/create')){
            throw new ForbiddenHttpException('Sizga ruxsat berilmagan!');
        }

        $type = DepartmentType::find()
            ->asArray()
            ->all();
        $parent = Department::find()
            ->asArray()
            ->where(['parent_id' => null])
            ->all();
        $currency_int = Currency::find()
            ->asArray()
            ->all();

        $depart = Department::find()
            ->asArray()
            ->where(['parent_id' => 0])
            ->all();

        $model = new Department();

        if ($model->load(Yii::$app->request->post())) {
            if(empty($model->parent_id)) $model->parent_id = 0;
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'type' => $type,
            'parent' => $parent,
            'currency_int' => $currency_int,
            'depart' => $depart
        ]);
    }

    public function actionUpdate($id)
    {
        if(!Yii::$app->user->can('department/update')){
            throw new ForbiddenHttpException('Sizga ruxsat berilmagan!');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Department model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if(!Yii::$app->user->can('department/delete')){
            throw new ForbiddenHttpException('Sizga ruxsat berilmagan!');
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Department model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Department the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Department::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
