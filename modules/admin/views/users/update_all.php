<?php
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Html;
    ?>
<div class="container">
    <div class="row">
        <table class="table" style="width: 70%">
            <thead>
                <th>ID</th><th>ITEM_NAME</th><th>user_id</th>
            </thead>
            <tbody>
            <form action="" data-id="<?=$_GET['id']?>" id="selection" method="post">
                <?php $i = 1; foreach($res as $r): ?>
                    <tr>
                        <td><?=$i?></td>
                        <td><select name="item_name" id="" class="form-control item_name">
                                <?php foreach ($item as $row): ?>
                                    <option <?php
                                        if($r['item_name'] === $row['name']) echo 'selected'
                                    ?>><?=$row["name"]?></option>
                                <?php endforeach; ?>
                            </select></td>
                        <td>
                            <select name="user_id" id="" class="form-control user_id">
                            <?php foreach ($user as $row): ?>
                                <option value="<?=$row['id']?>" <?php
                                if($r['user_id'] === $row['id']) echo 'selected'
                                ?>><?=$row['name']?></option>
                            <?php endforeach; ?>
                            </select></td>
                    </tr>
                <?php $i++; endforeach; ?>
                <input type="submit" name="ok" id="myButton" value="Update" class="btn btn-info btn-sm">
            </form>
            </tbody>
            <tfoot>

            </tfoot>
        </table>
    </div>
</div>

