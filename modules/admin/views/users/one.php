<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\grid\GridView;

    $dataProvider = new \yii\data\SqlDataProvider([
       'sql' => "SELECT * FROM auth_assignment WHERE user_id = $id",
    ]);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'showHeader' => true,
        'columns' => [
            'item_name',
            [
                'attribute' => 'item_name',
                'label' => 'delete',
                'format' => 'raw',
                'value' => function($res){
                    return '<input type="checkbox" value="'.$res['item_name'].'" class="items">';
                }
            ],
            [
                'attribute' => 'item_name',
                'label' => 'update',
                'format' => 'raw',
                'value' => function($res){
                    return '<input type="checkbox" value="'.$res['item_name'].'" class="items-update">';
                }
            ],

        ],
    ]);
    ?>
    <button class="btn btn-info btn-sm" data-id="<?=$id?>" id="delItems">Delete</button>
    <button class="btn btn-success btn-sm" data-id="<?=$id?>" id="updateItems">Update</button>
