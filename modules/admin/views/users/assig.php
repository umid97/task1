<?php
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Html;

    $f = ActiveForm::begin();
        echo $f->field($model, 'item_name')->widget(\kartik\select2\Select2::className(),[
            'data' => \yii\helpers\ArrayHelper::map($item, 'name', 'name')
        ]);
        echo $f->field($model, 'user_id')->widget(\kartik\select2\Select2::className(),[
            'data' => \yii\helpers\ArrayHelper::map($users, 'id', 'name'),
        ]);
        echo Html::submitButton('Crud', ['class' => 'btn btn-info btn-sm']);
    ActiveForm::end();