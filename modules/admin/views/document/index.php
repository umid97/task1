<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$token = Yii::$app->request->get('token', $searchModel::DOC_TYPE_KIRIM);
$this->title = "Documents {$token}";
?>
<div class="document-index">
    <p>
        <?= Html::a('Create Document', ["create?token={$token}"], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'doc_number',
            'reg_date',

            [
                'label' => 'Qaysi Bolimga',
                'attribute' => 'to_department',
                'format' => 'raw',
                'value' => function($res){
                    $token = $res->doc_type_id;
                    $r = \app\modules\admin\models\DocumentType::findOne($token);
                    if($r->token === 'KIRIM'){
                        $r = \app\modules\admin\models\Department::findOne($res->to_department);
                        return $r->name;
                    }
                    elseif($r->token === 'CHIQIM'){
                        $r = \app\modules\admin\models\Department::findOne($res->from_department);
                        return $r->name;
                    }
                }
            ],
            //'contragent_id',
            //'doc_type_id',
            //'status',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                     'view' => function($url, $data){
                            $token = Yii::$app->request->get('token');
                            $url = $url.'&token='.$token;
                            return '<a href="'.$url.'" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-eye-open"></i></a>';
                     },
                    'delete' => function($url, $data){
                        if($data->status != 3){
                            $token = Yii::$app->request->get('token');
                            $url = $url.'&token='.$token;
                            return '<a href="'.$url.'" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i></a>';
                        }
                    },
                    'update' => function($url, $data){
                        if($data->status != 3){
                            $token = Yii::$app->request->get('token');
                            $url = $url.'&token='.$token;
                            return '<a href="'.$url.'" class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-pencil"></i></a>';
                        }
                    }
                ]
            ],
        ],
    ]); ?>


</div>
