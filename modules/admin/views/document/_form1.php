<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use unclead\multipleinput\TabularInput;
    use unclead\multipleinput\MultipleInput;
    use kartik\select2\Select2;


    /* @var $this yii\web\View */
    /* @var $model app\modules\admin\models\Document */
    /* @var $models app\modules\admin\models\Document[] */
    /* @var $form yii\widgets\ActiveForm */
    ?>

        <div class="document-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'doc_number')->textInput() ?>

    <?= $form->field($model, 'reg_date')->textInput(['value' => date('Y-m-d H:i:s')]); ?>

    <?php
    echo $form->field($model, 'from_department')->widget(Select2::classname(), [
        'data' => $model->getDepartment(1),
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('From Department');
    ?>

    <?php
    echo $form->field($model, 'to_department')->widget(Select2::classname(), [
        'data' => $model->getDepartment(1),
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('To Department');
    ?>

    <?php
    echo $form->field($model, 'contragent_id')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map($contragent, 'id', 'name'),
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('From Contragent');
    ?>

    <?php
    echo $form->field($model, 'doc_type_id')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map($document_type, 'id', 'token'),
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Document Type');
    ?>

    <?= TabularInput::widget([
    'models' => $models,
    'attributeOptions' => [
    ],
    'columns' => [
        [
            'name'  => 'item_id',
            'type' => Select2::className(),
            'options' => [
                'class' => 'price',
                'data' => \yii\helpers\ArrayHelper::map(\app\modules\admin\models\Item::find()->asArray()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Maxsulotni biriktiring....',
                ],
                'pluginOptions' => [
                    'allowclear' => true,
                ]
            ],
            'title' => Yii::t('app', 'Item Id'),
        ],
        [
            'name'  => 'quantity',
            'title' => Yii::t('app', 'Quantity'),
        ],
        [
            'name'  => 'income_price',
            'title' => Yii::t('app', 'Income price'),
        ],
        [
            'name'  => 'wh_price',
            'title' => Yii::t('app', 'Wh price'),
        ],
        [
            'name'  => 'selling_price',
            'title' => Yii::t('app', 'Selling price'),
        ],
        [
            'name'  => 'currency',
            'title' => Yii::t('app', 'Currency'),
            'type' => Select2::className(),
            'options' => [
                'class' => 'price',
                'data' => \yii\helpers\ArrayHelper::map(\app\modules\admin\models\Currency::find()->asArray()->all(), 'id', 'token'),
                'options' => [
                    'placeholder' => 'Valyutani biriktiring....',
                ],
                'pluginOptions' => [
                    'allowclear' => true,
                ]
            ],
        ],
        [
            'name'  => 'lot',
            'title' => Yii::t('app', 'Lot'),
        ],

    ],
]) ?>
    <?=Html::submitButton('Update', ['class' => 'btn btn-info btn-sm'])?>
<?php
    ActiveForm::end();

?>