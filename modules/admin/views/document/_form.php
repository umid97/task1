<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\TabularInput;
use yii\widgets\MaskedInput;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Document */
/* @var $models app\modules\admin\models\Document[] */
/* @var $form yii\widgets\ActiveForm */

$token = $this->context->token;
?>

<div class="document-form">
    <?=$token; ?>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'doc_number')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'reg_date')->textInput(['value' => date('Y-m-d H:i:s')]); ?>
        </div>
    </div>
    <?php
    if($token == $model::DOC_TYPE_KIRIM){
        echo $this->render('_kirim', ['model' => $model,'form' => $form]);
    }
    elseif($token == $model::DOC_TYPE_CHIQIM){
        echo $this->render('_chiqim', ['model' => $model,'form' => $form,]);
    }
    elseif($token == $model::DOC_TYPE_KOCHIRISH){
        echo $this->render('_kochirish', ['model' => $model,'form' => $form,]);
    }
    else{
        throw new \yii\web\ForbiddenHttpException('Siz yoq sahifani qidiryabsiz!');
    }
    ?>
    <?= TabularInput::widget([
        'models' => $models,
        'attributeOptions' => [
        ],
        'columns' => [
            [
                'name'  => 'item_id',
                'type' => Select2::className(),
                'options' => [
                    'class' => 'price',
                    'data' => \yii\helpers\ArrayHelper::map(\app\modules\admin\models\Item::find()->asArray()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Maxsulotni biriktiring....',
                    ],
                    'pluginOptions' => [
                         'allowclear' => true,
                    ]
                ],
                'title' => Yii::t('app', 'Item Id'),
            ],
            [
                'name'  => 'quantity',
                'title' => Yii::t('app', 'Quantity'),
            ],
            [
                'name'  => 'income_price',
                'title' => Yii::t('app', 'Income price'),
            ],
            [
                'name'  => 'wh_price',
                'title' => Yii::t('app', 'Wh price'),
            ],
            [
                'name'  => 'selling_price',
                'title' => Yii::t('app', 'Selling price'),
            ],
            [
                'name'  => 'currency',
                'title' => Yii::t('app', 'Currency'),
                'type' => Select2::className(),
                'options' => [
                    'class' => 'price',
                    'data' => \yii\helpers\ArrayHelper::map(\app\modules\admin\models\Currency::find()->asArray()->all(), 'id', 'token'),
                    'options' => [
                        'placeholder' => 'Valyutani biriktiring....',
                    ],
                    'pluginOptions' => [
                        'allowclear' => true,
                    ]
                ],
            ],
            [
                'name'  => 'lot',
                'title' => Yii::t('app', 'Lot'),
            ],

        ],
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
