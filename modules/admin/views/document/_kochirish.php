<?php
use kartik\select2\Select2;
?>
<div class="row">
    <div class="col-md-6">
        <?php
        echo $form->field($model, 'from_department')->widget(Select2::classname(), [
            'data' => $model->getDepartment(1),
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('From Department');
        ?>
    </div>
    <div class="col-md-6">
        <?php
        echo $form->field($model, 'to_department')->widget(Select2::classname(), [
            'data' => $model->getDepartment(1),
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('To Department');
        ?>
    </div>
</div>




