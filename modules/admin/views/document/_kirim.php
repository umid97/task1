<?php
use kartik\select2\Select2;
?>
<div class="row">
    <div class="col-md-6">
        <?php
        echo $form->field($model, 'contragent_id')->widget(Select2::classname(), [
            'data' => $model->getContragents(1),
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('From Contragent');
        ?>
    </div>
    <div class="col-md-6">
        <?php
        echo $form->field($model, 'to_department')->widget(Select2::classname(), [
            'data' => $model->getDepartment(1),
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('To Department');
        ?>
    </div>
</div>
<?php
$form->field($model, 'from_department')->widget(Select2::classname(), [
    'data' => $model->getDepartment(1),
    'options' => ['placeholder' => 'Select a state ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
])->label('From Department');
?>



