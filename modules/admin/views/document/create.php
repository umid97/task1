<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Document */
/* @var $models app\modules\admin\models\Document[] */

$this->title = 'Create Document';
$this->params['breadcrumbs'][] = ['label' => 'Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-create">
    <?= $this->render('_form', [
        'model' => $model,
        'document_type' => $document_type,
        'models' => $models,
    ]) ?>

</div>
