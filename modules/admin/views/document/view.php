<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Document */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="document-view">
    <p>
        <?php
            $token1 = \app\modules\admin\models\DocumentType::findOne($model->doc_type_id);
            if($model->status == $model::STATUS_ACTIVE):
        ?>
            <?= Html::a('Yakunlash', ['finish', 'id' => $model->id, 'token' => $token1->token], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Update', ['update', 'id' => $model->id, 'token' => $token1->token], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id, 'token' => $token1->token], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]);
//            debug($model);exit();
            ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'doc_number',
            'reg_date',
            [
                'attribute' => 'From_department',
                'label' => 'From Department',
                'format' => 'raw',
                'value' => function($res){
                    $token = $res->doc_type_id;
                    $r = \app\modules\admin\models\DocumentType::findOne($token);
                    if($r->token === 'KIRIM'){
                        $r = \app\modules\admin\models\Department::findOne($res->to_department);
                        return $r->name;
                    }
                    elseif($r->token === 'CHIQIM'){
                        $r = \app\modules\admin\models\Department::findOne($res->from_department);
                        return $r->name;
                    }
                    elseif($r->token === 'KOCHIRISH'){
                        $r = \app\modules\admin\models\Department::findOne($res->from_department);
                        $r1 = \app\modules\admin\models\Department::findOne($res->to_department);
                        return $r->name.' -> '.$r1->name;
                    }
                }
            ],

            [
                'attribute' => 'contragent_id',
                'label' => 'contragent_id',
                'format' => 'raw',
                'value' => function($res){
                    if(!empty($res->contragent_id))
                    {
                        $token = \app\modules\admin\models\Contragent::findOne($res->contragent_id);
                        return $token->name;
                    }
                }
            ],
            [
                'attribute' => 'doc_type_id',
                'label' => 'DocType',
                'format' => 'raw',
                'value' => function($res){
                    $r = \app\modules\admin\models\DocumentType::findOne($res->doc_type_id);
                    return $r->token;
                }
            ],
            'status',
        ],
    ]) ?>

    <hr>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Maxsulot nomi</th>
                <th>Soni</th>
                <th>Daromad Narxi</th>
                <th>Harajat Narxi</th>
                <th>Sotish Narxi</th>
                <th>Valyuta</th>
                <th>Lot</th>
                <th>document_id</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $r): ?>
                <tr>
                    <td>
                        <?php
                            $row = \app\modules\admin\models\Item::findOne(['id' => $r['item_id']]);
                            echo $row->name;
                        ?>
                    </td>
                    <td><?=$r['quantity']?></td>
                    <td><?=$r['income_price']?></td>
                    <td><?=$r['wh_price']?></td>
                    <td><?=$r['selling_price']?></td>
                    <td><?php
                            $d = \app\modules\admin\models\Currency::findOne($r['currency']);
                            echo $d['token'];
                        ?></td>
                    <td><?=$r['lot']?></td>
                    <td><?=$r['document_id']?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot></tfoot>
    </table>

</div>
