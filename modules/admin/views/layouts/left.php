<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?=Yii::$app->user->identity->name?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>

            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    [
                            'label' => 'Users',
                            'icon' => 'users',
                            'url' => ['users/index'],
                            'visible' => (Yii::$app->user->can("department/index"))
                    ],
                    ['label' => 'Department', 'icon' => 'dashboard', 'url' => ['department/index']],
                    ['label' => 'Item', 'icon' => 'dashboard', 'url' => ['item/index']],
                    ['label' => 'ItemCategory', 'icon' => 'book', 'url' => ['item-category/index']],
                    ['label' => 'Unit', 'icon' => 'book', 'url' => ['unit/index']],
                    ['label' => 'Contragent', 'icon' => 'book', 'url' => ['contragent/index']],
                    ['label' => 'DocumentType', 'icon' => 'dashboard', 'url' => ['document-type/index']],
                    ['label' => 'UserDepartment', 'icon' => 'user', 'url' => ['user-department/index']],
                    ['label' => 'DepartmentType', 'icon' => 'user', 'url' => ['department-type/index']],
                    ['label' => 'Currency', 'icon' => 'user', 'url' => ['currency/index']],
                    ['label' => 'AuthAssignment', 'icon' => 'book', 'url' => ['auth-assignment/index']],
                    ['label' => 'AuthItem', 'icon' => 'book', 'url' => ['auth-item/index']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Documents',
                        'icon' => 'list',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Kirim', 'icon' => 'arrow-down', 'url' => ['/admin/document/index?token=KIRIM'],],
                            ['label' => 'Chiqim', 'icon' => 'arrow-up', 'url' => ['/admin/document/index?token=CHIQIM'],],
                            ['label' => 'Kochirish', 'icon' => 'arrows-h', 'url' => ['/admin/document/index?token=KOCHIRISH'],],
                        ],
                    ],
                    [
                        'label' => 'Some tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
