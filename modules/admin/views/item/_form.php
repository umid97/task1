<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\mywidget\MySelect;
?>

<div class="item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>


    <?=MySelect::widget(['obj' => $form, 'model' => $model, 'name' => 'category_id', 'data' => $item, 'col1' => 'id', 'col2' => 'name', 'bool' => false, 'status' => 'Tanlang...'])?>

    <?= $form->field($model, 'wieght')->textInput() ?>

    <?=MySelect::widget(['obj' => $form, 'model' => $model, 'name' => 'unit_id', 'data' => $unit, 'col1' => 'id', 'col2' => 'token', 'bool' => false, 'status' => 'Tanlang...'])?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
