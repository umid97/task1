<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\AuthAssignment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-assignment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'item_name')->widget(\kartik\select2\Select2::className(),[
        'data' => \yii\helpers\ArrayHelper::map($item, 'name', 'name')
    ]); ?>

    <?= $form->field($model, 'user_id')->widget(\kartik\select2\Select2::className(),[
        'data' => \yii\helpers\ArrayHelper::map($user, 'id', 'name')
    ]);  ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
