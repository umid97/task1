<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\mywidget\MySelect;
?>

<div class="user-department-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=MySelect::widget(['obj' => $form, 'model' => $model, 'name' => 'user_id', 'data' => $users, 'col1' => 'id', 'col2' => 'name', 'bool' => false, 'status' => 'Tanlang...'])?>


    <?=MySelect::widget(['obj' => $form, 'model' => $model, 'name' => 'department_id', 'data' => $dep, 'col1' => 'id', 'col2' => 'name', 'bool' => false, 'status' => 'Tanlang...'])?>

    <?= $form->field($model, 'to_department')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
