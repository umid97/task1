<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\mywidget\MySelect;
?>

<div class="department-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_id')->widget(\kartik\select2\Select2::className(),[
            'data' => \yii\helpers\ArrayHelper::map($type, 'id', 'name'),
            'options' => ['placeholder' => 'Select states ...']
    ]) ?>

    <?= $form->field($model, 'parent_id')->widget(\kartik\select2\Select2::className(),[
        'data' => \yii\helpers\ArrayHelper::map($depart, 'id', 'name'),
        'options' => ['placeholder' => 'Select states ...']
    ]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?=MySelect::widget(['obj' => $form, 'model' => $model, 'name' => 'international_currency', 'data' => $currency_int, 'col1' => 'id', 'col2' => 'token', 'bool' => false, 'status' => 'Tanlang...'])?>

    <?=MySelect::widget(['obj' => $form, 'model' => $model, 'name' => 'default_currency', 'data' => $currency_int, 'col1' => 'id', 'col2' => 'token', 'bool' => false, 'status' => 'Tanlang...'])?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
